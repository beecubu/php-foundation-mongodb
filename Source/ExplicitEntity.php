<?php

namespace Beecubu\Foundation\MongoDB;

/**
 * Especifica una classe del tipus Entity però que ja vé configurada amb la opció de serialitzar el nom de la classe.
 */
abstract class ExplicitEntity extends Entity
{
    protected $serializeClassName = true;
}