<?php

namespace Beecubu\Foundation\MongoDB\Exceptions;

use Beecubu\Foundation\MongoDB\Entity;
use Exception;

/**
 * Quan s'intenta convertir un Id a ObjectId i aquest no és vàlid.
 */
class InvalidObjectIdException extends Exception
{
    public function __construct(string $value, string $who)
    {
        parent::__construct("Error: '".$value."' is not a valid ObjectId for class '".$who."'");
    }
}
