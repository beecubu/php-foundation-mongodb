<?php

namespace Beecubu\Foundation\MongoDB;

/**
 * Especifica una classe del tipus Entity però que els seus IDs no són els del Mongo.
 */
abstract class PlainEntity extends Entity
{
    /**
     * Converteix un property normal a MongoId.
     *
     * @param mixed $id El valor del property a convertir.
     *
     * @return mixed L'id convertit a MongoId.
     */
    protected static function __idToRawData($id)
    {
        return $id;
    }

    /**
     * Converteix un MongoId a un property.
     *
     * @param mixed $id L'id a convertir a property.
     *
     * @return mixed El MongoId convertit a string.
     */
    protected static function __idFromRawData($id)
    {
        return $id;
    }
}
