<?php

namespace Beecubu\Foundation\MongoDB\Driver;

use MongoDB\Driver\Manager;

class MongoClient
{
    /** @var Manager */
    private $connection = null;

    public function __construct($uri = "mongodb://localhost:27017", array $options = [], array $driverOptions = [])
    {
        if ( ! preg_match('|(.*?)://|', $uri))
        {
            $uri = 'mongodb://'.$uri;
        }
        // connect to mongo server
        $this->connection = new Manager($uri, $options, $driverOptions);
    }

    /**
     * @param string $name Database name.
     *
     * @return MongoDB
     */
    public function __get($name)
    {
        return new MongoDB($this, $name);
    }

    /**
     * @return Manager
     */
    public function getManager()
    {
        return $this->connection;
    }
}
