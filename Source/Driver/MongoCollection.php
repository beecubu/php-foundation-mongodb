<?php

namespace Beecubu\Foundation\MongoDB\Driver;

use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Command;
use MongoDB\Driver\Cursor;
use MongoDB\Driver\Query;
use MongoDB\Driver\WriteResult;
use stdClass;

class MongoCollection
{
    /** @var MongoDB $db */
    private $db = null;
    /** @var string $collection */
    private $collection = null;

    public function __construct(MongoDB $db, $collection)
    {
        $this->db = $db;
        $this->collection = $collection;
    }

    /**
     * Selects documents in a collection or view and returns a cursor to the selected documents.
     *
     * @param array|stdClass $query The fields for which to search.
     * @param array $fields Fields of the results to return.
     * @param array $options This parameter is an associative array of the form array("name" => <value>, ...).
     *
     * @return Cursor A cursor to the documents that match the query criteria. When the find() method “returns documents,” the method is actually returning a cursor to the documents.
     *
     * @throws
     */
    public function find($query = [], array $fields = [], array $options = [])
    {
        if ( ! empty($fields))
        {
            $options['projection'] = $fields;
        }
        // execute the mongo query
        return $this->db->getMongoClient()->getManager()->executeQuery($this->getNamespace(), new Query($query, $options));
    }

    /**
     * Returns one document that satisfies the specified query criteria on the collection or view. If multiple documents satisfy the query,
     * this method returns the first document according to the natural order which reflects the order of documents on the disk. In capped
     * collections, natural order is the same as insertion order. If no document satisfies the query, the method returns null.
     *
     * @param array|stdClass $query Optional. Specifies query selection criteria using query operators.
     * @param array $fields Optional. Specifies the fields to return using projection operators. Omit this parameter to return all fields in the matching document.
     * @param array $options This parameter is an associative array of the form array("name" => <value>, ...).
     *
     * @return stdClass One document that satisfies the criteria specified as the first argument to this method. If you specify a projection parameter,
     *                  findOne() returns a document that only contains the projection fields. The _id field is always included unless you explicitly exclude it.
     */
    public function findOne($query = [], array $fields = [], array $options = [])
    {
        if ($cursor = $this->find($query, $fields, $options + ['limit' => 1]))
        {
            if ($result = $cursor->toArray())
            {
                return $result[0];
            }
        }
        return null;
    }

    /**
     * Modifies and returns a single document. By default, the returned document does not include the modifications made on the update.
     * To return the document with the modifications made on the update, use the new option.
     *
     * @param array|stdClass $query The query criteria to search for.
     * @param array|stdClass $update The update criteria.
     * @param array $fields Optionally only return these fields.
     * @param array $options This parameter is an associative array of the form array("name" => <value>, ...).
     *
     * @return array|stdClass|null Returns the original document, or the modified document when new is set.
     *
     * @throws
     */
    public function findAndModify($query, $update, array $fields = [], array $options = [])
    {
        // prepare command document
        $cmd = ['findAndModify' => $this->collection, 'query' => $query, 'update' => $update];
        // assign the fields if is not empty
        if ( ! empty($fields)) $cmd['fields'] = $fields;
        // append the "other" options
        $cmd = array_merge($cmd, $options);
        // execute command
        $cursor = $this->db->getMongoClient()->getManager()->executeCommand($this->db->getDatabaseName(), new Command($cmd));
        // get the first result
        if ($result = $cursor->toArray())
        {
            // is all ok?
            if ($result[0]->ok)
            {
                return $result[0]->value;
            }
        }
        // buuuu
        return null;
    }

    /**
     * Inserts a document or documents into a collection.
     *
     * @param array|stdClass $document A document or array of documents to insert into the collection.
     * @param array $options This parameter is an associative array of the form array("name" => <value>, ...).
     *
     * @return WriteResult
     */
    public function insert($document, array $options = [])
    {
        $bulk = new BulkWrite($options);
        // insert
        $_id = $bulk->insert($document);
        // execute write command
        $result = $this->db->getMongoClient()->getManager()->executeBulkWrite($this->getNamespace(), $bulk);
        // update the collection _id
        if (is_array($document))
        {
            $document['_id'] = $_id;
        }
        else
        {
            $document->_id = $_id;
        }
        // all is ok
        return $result;
    }

    /**
     * Modifies an existing document or documents in a collection. The method can modify specific fields of an existing document or documents or
     * replace an existing document entirely, depending on the update parameter.
     *
     * By default, the update() method updates a single document. Set the Multi Parameter to update all documents that match the query criteria.
     *
     * @param array|stdClass $query The selection criteria for the update. The same query selectors as in the find() method are available.
     * @param array|stdClass $document The modifications to apply. For details see Update Parameter.
     * @param array $options This parameter is an associative array of the form array("name" => <value>, ...).
     *
     * @return WriteResult
     */
    public function update($query, $document, array $options = [])
    {
        $bulk = new BulkWrite($options);
        // update
        $bulk->update($query, $document, $options);
        // execute write command
        return $this->db->getMongoClient()->getManager()->executeBulkWrite($this->getNamespace(), $bulk);
    }

    /**
     * Updates an existing document or inserts a new document, depending on its document parameter.
     *
     * @param array|stdClass $document A document to save to the collection.
     * @param array $options This parameter is an associative array of the form array("name" => <value>, ...).
     *
     * @return WriteResult
     */
    public function save($document, array $options = [])
    {
        if ($_id = $document ? (is_array($document) ? $document['_id'] ?? null : $document->_id ?? null) : null)
        {
            $options += ['upsert' => true];
            // execute the update command
            return $this->update(['_id' => $_id], $document, $options);
        }
        // no id, so insert it
        return $this->insert($document, $options);
    }

    /**
     * Removes documents from a collection.
     *
     * @param array|stdClass $query Specifies deletion criteria using query operators. To delete all documents in a collection, pass an empty document ({}).
     * @param array $options This parameter is an associative array of the form array("name" => <value>, ...).
     *
     * @return WriteResult
     */
    public function remove($query, array $options = [])
    {
        $bulk = new BulkWrite($options);
        // update
        $bulk->delete($query, $options);
        // execute write command
        return $this->db->getMongoClient()->getManager()->executeBulkWrite($this->getNamespace(), $bulk);
    }

    /**
     * The MongoDB {@link http://docs.mongodb.org/manual/applications/aggregation/ aggregation framework}
     * provides a means to calculate aggregated values without having to use
     * MapReduce. While MapReduce is powerful, it is often more difficult than
     * necessary for many simple aggregation tasks, such as totaling or averaging
     * field values.
     *
     * @link http://php.net/manual/en/mongocollection.aggregatecursor.php
     *
     * @param array|stdClass $pipeline The Aggregation Framework pipeline to execute.
     * @param array $options This parameter is an associative array of the form array("name" => <value>, ...).
     *
     * @return Cursor A cursor to the documents that match the query criteria. When the aggregate() method “returns documents,” the method is actually returning a cursor to the documents.
     *
     * @throws
     */
    public function aggregate($pipeline, array $options = [])
    {
        if ( ! is_array($options))
        {
            $options = [];
        }
        // create command
        $cmd = ['aggregate' => $this->collection, 'pipeline' => $pipeline];
        // is the cursor option not defined? then auto-assign a value
        if ( ! isset($options['cursor']))
        {
            $options['cursor'] = isset($options['batchSize']) ? ['batchSize' => $options['batchSize']] : new stdClass;
        }
        // merge cmd and options
        $cmd = array_merge($cmd, $options);
        // execute command
        return $this->db->getMongoClient()->getManager()->executeCommand($this->db->getDatabaseName(), new Command($cmd));
    }

    /**
     * Finds the distinct values for a specified field across a single collection or view and returns the results in an array.
     *
     * @param string $field The field for which to return distinct values.
     * @param array|stdClass $query A query that specifies the documents from which to retrieve the distinct values.
     * @param array $options This parameter is an associative array of the form array("name" => <value>, ...).
     *
     * @return array|null The result of the distinct operation.
     *
     * @throws
     */
    public function distinct($field, $query = null, array $options = [])
    {
        $cmd = ['distinct' => $this->collection, 'key' => $field, 'query' => $query, 'options' => $options];
        // execute command
        $cursor = $this->db->getMongoClient()->getManager()->executeCommand($this->db->getDatabaseName(), new Command($cmd));
        // get the first result
        if ($result = $cursor->toArray())
        {
            // is all ok?
            if ($result[0]->ok)
            {
                return $result[0]->values;
            }
        }
        // buuuu
        return null;
    }

    /**
     * Returns the count of documents that would match a find() query for the collection or view. The db.collection.count() method does
     * not perform the find() operation but instead counts and returns the number of results that match a query.
     *
     * @param array|stdClass $query The query selection criteria.
     *
     * @return integer Returns the number of documents matching the query.
     *
     * @throws
     */
    public function count($query = [])
    {
        $cmd = new Command(['count' => $this->collection, 'query' => $query]);
        $cursor = $this->db->getMongoClient()->getManager()->executeCommand($this->db->getDatabaseName(), $cmd);
        // get the first result
        if ($result = $cursor->toArray())
        {
            // is all ok?
            if ($result[0]->ok)
            {
                return $result[0]->n;
            }
        }
        // buuuu
        return 0;
    }

    /**
     * Return if this collection exists or not.
     *
     * @return bool Returns TRUE if exists, FALSE if not.
     *
     * @throws
     */
    public function exists()
    {
        $cmd = new Command(['listCollections' => 1, 'filter' => ['name' => $this->collection]]);
        $result = $this->db->getMongoClient()->getManager()->executeCommand($this->db->getDatabaseName(), $cmd)->toArray();
        return ! empty($result);
    }

    /**
     * Removes a collection or view from the database. The method also removes any indexes associated with the dropped collection. The method provides a wrapper around the drop command.
     *
     * @return boolean TRUE when successfully drops a collection, FALSE when collection to drop does not exist.
     *
     * @throws
     */
    public function drop()
    {
        if ($this->exists())
        {
            $cmd = new Command(['drop' => $this->collection]);
            $cursor = $this->db->getMongoClient()->getManager()->executeCommand($this->db->getDatabaseName(), $cmd);
            // get the first result
            if ($result = $cursor->toArray())
            {
                return $result[0]->ok === 1;
            }
        }
        // buuuu
        return false;
    }

    /**
     * Current namespace (db.collection)
     *
     * @return string
     */
    public function getNamespace()
    {
        return $this->db->getDatabaseName().'.'.$this->collection;
    }
}
