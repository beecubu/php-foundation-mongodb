<?php

namespace Beecubu\Foundation\MongoDB\Driver;

class MongoDB
{
    /** @var MongoClient $client */
    private $client = null;
    /** @var string $name */
    private $name = null;

    public function __construct(MongoClient $client, $name)
    {
        $this->client = $client;
        $this->name = $name;
    }

    /**
     * @param string $name
     *
     * @return MongoCollection
     */
    public function __get($name)
    {
        return new MongoCollection($this, $name);
    }

    /**
     * The current database name.
     *
     * @return string
     */
    public function getDatabaseName()
    {
        return $this->name;
    }

    /**
     * @return MongoClient
     */
    public function getMongoClient()
    {
        return $this->client;
    }
}
