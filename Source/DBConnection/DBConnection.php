<?php

namespace Beecubu\Foundation\MongoDB\DBConnection;

use Beecubu\Foundation\Core\Objectum;
use Beecubu\Foundation\MongoDB\Driver\MongoClient;

defined('MONGO_DATABASE') or define('MONGO_DATABASE', 'default');
defined('MONGO_SERVER') or define('MONGO_SERVER', 'localhost:27017');
defined('MONGO_DATABASE_SANDBOX_SUFIX') or define('MONGO_DATABASE_SANDBOX_SUFIX', '_sandbox');

/**
 * La connexió amb la Base de Dades.
 */
class DBConnection extends Objectum
{
    /** @var MongoClient|null */
    protected static $connection = null;

    protected $db;

    /**
     * Crea una nova connexió.
     */
    public function __construct(?string $dataBaseOverride = null)
    {
        parent::__construct();
        // init class
        if ( ! self::$connection)
        {
            self::$connection = new MongoClient(MONGO_SERVER);
        }
        // connect to alternative DB
        if ($dataBaseOverride)
        {
            $this->db = self::$connection->$dataBaseOverride;
        }
        else // configure the "default" DB
        {
            // get the current configured database (default = MONGO_DATABASE)
            $dataBase = MONGO_DATABASE.($this->isSandboxMode() ? MONGO_DATABASE_SANDBOX_SUFIX : '');
            // connect to database
            $this->db = self::$connection->$dataBase;
        }
    }

    /**
     * Retorna si esta en modo sandbox o no.
     *
     * @return bool
     */
    protected function isSandboxMode(): bool
    {
        return false;
    }
}
