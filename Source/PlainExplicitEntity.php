<?php

namespace Beecubu\Foundation\MongoDB;

/**
 * Especifica una classe del tipus PlainEntity però que ja vé configurada amb la opció de serialitzar el nom de la classe.
 */
abstract class PlainExplicitEntity extends PlainEntity
{
    protected $serializeClassName = true;
}