<?php

namespace Beecubu\Foundation\MongoDB;

use Beecubu\Foundation\Core\Serializable;
use Beecubu\Foundation\Core\Tools\DateTime;
use Beecubu\Foundation\MongoDB\Exceptions\InvalidObjectIdException;
use MongoDB\BSON\Binary;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use stdClass;
use function Beecubu\Foundation\Helpers\Id\validateObjectId;

/**
 * Implementació de la serialització a MongoDB.
 */
abstract class Entity extends Serializable
{
    protected static $ivar_internalId = '_id';

    /**
     * Converteix un property normal a ObjectId.
     *
     * @param mixed $id El valor del property a convertir.
     *
     * @return ObjectId L'id convertit a ObjectId.
     *
     * @throws InvalidObjectIdException
     */
    protected static function __idToRawData($id)
    {
        if (is_string($id) && validateObjectId($id))
        {
            return new ObjectId($id);
        }
        elseif ($id instanceof ObjectId)
        {
            return $id;
        }
        throw new InvalidObjectIdException($id, static::class);
    }

    /**
     * Converteix un ObjectId a un property.
     *
     * @param mixed $id L'id a convertir a property.
     *
     * @return string El ObjectId convertit a string.
     */
    protected static function __idFromRawData($id)
    {
        return (string)$id;
    }

    /**
     * Converteix un DateTime al format necessari en RawData.
     *
     * @param \DateTime $dateTime La data a convertir.
     *
     * @return UTCDateTime La data convertida.
     */
    protected static function __dateTimeToRawData(\DateTime $dateTime): UTCDateTime
    {
        return new UTCDateTime((float)$dateTime->format('Uv'));
    }

    /**
     * Converteix un valor UTCDateTime a DateTime.
     *
     * @param mixed $dateTime El UTCDateTime a convertir a DateTime.
     *
     * @return DateTime El valor convertit a DateTime.
     *
     * @throws
     */
    protected static function __dateTimeFromRawData($dateTime): DateTime
    {
        return new DateTime('@'.$dateTime->toDateTime()->format('U.u'));
    }

    /**
     * Determina si un objecte és del tipus UTCDateTime RawData.
     *
     * @param mixed $value El valor a comprovar.
     *
     * @return boolean TRUE = Ho és, FALSE = no.
     */
    protected static function __isRawDateTimeInstance($value): bool
    {
        return $value instanceof UTCDateTime;
    }

    /**
     * Converteix informació binària al format necessari en RawData.
     *
     * @param string $data La informació a convertir en Binary.
     *
     * @return Binary | null La informació convertida en Binary.
     */
    protected static function __binaryDataToRawData($data): ?Binary
    {
        if (is_string($data))
        {
            return new Binary($data, Binary::TYPE_GENERIC);
        }
        return null;
    }

    /**
     * Converteix un valor MongoBinData a mixed.
     *
     * @param mixed $data El MongoBinData a convertir a mixed.
     *
     * @return mixed El valor convertit a mixed.
     */
    protected static function __binaryDataFromRawData($data)
    {
        if (static::__isBinaryDataInstance($data))
        {
            return $data->getData();
        }
        return null;
    }

    /**
     * Determina si un objecte és del tipus Binary RawData.
     *
     * @param mixed $value El valor a comprovar.
     *
     * @return boolean TRUE = Ho és, FALSE = no.
     */
    protected static function __isBinaryDataInstance($value): bool
    {
        return $value instanceof Binary;
    }

    /**
     * @param $data
     *
     * @return mixed|ObjectId
     *
     * @throws InvalidObjectIdException
     */
    protected static function parseDataBeforeInsertInIvarsOnDemand($data)
    {
        if ($data instanceof ObjectId)
        {
            return $data;
        }
        elseif (is_string($data) && validateObjectId($data))
        {
            return static::__idToRawData($data);
        }
        elseif ($data instanceof stdClass || is_array($data))
        {
            $array = (array)$data;
            $ivar_publicId = static::$ivar_publicId;
            // is a string or ObjectId then "parse-again"
            if ($value = ($array[$ivar_publicId] ?? null) and ($value instanceof ObjectId || is_string($value)))
            {
                return static::parseDataBeforeInsertInIvarsOnDemand($value);
            }
        }
        // execute the default parser
        return parent::parseDataBeforeInsertInIvarsOnDemand($data);
    }
}
